package br.com.shazan.croqui.cocalzinho.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class WebSecurityConfig {

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .httpBasic()
                .and()
                .authorizeHttpRequests()
                .antMatchers(HttpMethod.GET, "/versao/**","/escalador/**","/local/**").permitAll()
                .antMatchers(HttpMethod.GET, "/swagger-ui.html/**").hasRole(Constants.PERFIL)
                .antMatchers(HttpMethod.POST, "/estudantes/**","/locais/**").hasRole(Constants.PERFIL)
                .antMatchers(HttpMethod.PUT, "/estudantes/**","/locais/**").hasRole(Constants.PERFIL)
                .antMatchers(HttpMethod.DELETE, "/estudantes/**","/locais/**").hasRole(Constants.PERFIL)
                .anyRequest().authenticated()
                .and()
                .csrf().disable();
                http.csrf().disable();
                http.headers().frameOptions().disable();
        return http.build();
    }
}
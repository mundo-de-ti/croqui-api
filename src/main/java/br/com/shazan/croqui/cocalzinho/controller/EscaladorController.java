package br.com.shazan.croqui.cocalzinho.controller;

import br.com.shazan.croqui.cocalzinho.model.Escalador;
import br.com.shazan.croqui.cocalzinho.repository.EscaladorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/")
public class EscaladorController {
    @Autowired
    EscaladorRepository escaladorRepository;
    @GetMapping("/escalador/lista")
    public ResponseEntity<List<Escalador>> getAllEscaladores(@RequestParam(required = false) String nome) {
        try {
            List<Escalador> escaladores = new ArrayList<>();
            if (nome == null)
                escaladorRepository.findAll().forEach(escaladores::add);
            else
                escaladorRepository.findByNomeContaining(nome).forEach(escaladores::add);
            if (escaladores.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(escaladores, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/escalador/{id}")
    public ResponseEntity<Escalador> getEscaladorById(@PathVariable("id") long id) {
        Optional<Escalador> escaladorData = escaladorRepository.findById(id);
        if (escaladorData.isPresent()) {
            return new ResponseEntity<>(escaladorData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/escaladores")
    public ResponseEntity<Escalador> createEscalador(@RequestBody Escalador escalador) {
        try {
            Escalador _escalador = escaladorRepository
                    .save(new Escalador(escalador.getNome(), escalador.getCidade(), escalador.getEmail()));
            return new ResponseEntity<>(_escalador, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PutMapping("/escaladores/{id}")
    public ResponseEntity<Escalador> updateEscalador(@PathVariable("id") long id, @RequestBody Escalador escalador) {
        Optional<Escalador> escaladorData = escaladorRepository.findById(id);
        if (escaladorData.isPresent()) {
            Escalador _escalador = escaladorData.get();
            _escalador.setNome(escalador.getNome());
            _escalador.setCidade(escalador.getCidade());
            _escalador.setEmail(escalador.getEmail());
            return new ResponseEntity<>(escaladorRepository.save(_escalador), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    @DeleteMapping("/escaladores/{id}")
    public ResponseEntity<HttpStatus> deleteEscalador(@PathVariable("id") long id) {
        try {
            escaladorRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @DeleteMapping("/escaladores")
    public ResponseEntity<HttpStatus> deleteAllEscaladores() {
        try {
            escaladorRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

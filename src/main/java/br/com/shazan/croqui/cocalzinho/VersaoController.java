package br.com.shazan.croqui.cocalzinho;

        import org.springframework.beans.factory.annotation.Value;
        import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.RequestMethod;
        import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("versao")
public class VersaoController {

    @Value("${versao.sistema}")
    private String versao;


    VersaoController() {
    }

    @RequestMapping(value = "/numero",method = RequestMethod.GET, produces = "application/json")
    public String versaoBackEnd(){
        return versao;
    }
}

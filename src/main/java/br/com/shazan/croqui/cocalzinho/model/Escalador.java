package br.com.shazan.croqui.cocalzinho.model;

import javax.persistence.*;
@Entity
@Table(name = "escalador")
public class Escalador {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "nome", nullable = false)
    private String nome;
    @Column(name = "cidade", nullable = false)
    private String cidade;
    @Column(name = "email", nullable = false)
    private String email;
    public Escalador() {
    }
    public Escalador(String nome, String cidade, String email) {
        this.nome = nome;
        this.cidade = cidade;
        this.email = email;
    }
    public Long getId() {
        return id;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getCidade() {
        return cidade;
    }
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Escalador [id=" + id + ", nome=" + nome + ", cidade=" + cidade + ", email=" + email + "]";
    }
}
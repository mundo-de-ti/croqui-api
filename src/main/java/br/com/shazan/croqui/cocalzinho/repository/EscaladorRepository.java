package br.com.shazan.croqui.cocalzinho.repository;

import br.com.shazan.croqui.cocalzinho.model.Escalador;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EscaladorRepository extends JpaRepository<Escalador, Long> {
    List<Escalador> findByEmail(String email);
    List<Escalador> findByNomeContaining(String nome);
}
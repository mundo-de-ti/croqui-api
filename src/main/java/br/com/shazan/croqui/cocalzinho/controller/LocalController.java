package br.com.shazan.croqui.cocalzinho.controller;

import br.com.shazan.croqui.cocalzinho.model.Local;
import br.com.shazan.croqui.cocalzinho.repository.LocalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/")
public class LocalController {
    @Autowired
    LocalRepository localRepository;
    @GetMapping("/local/lista")
    public ResponseEntity<List<Local>> getAllLocais(@RequestParam(required = false) String cidade) {
        try {
            List<Local> locais = new ArrayList<>();
            if (cidade == null)
                localRepository.findAll().forEach(locais::add);
            else
                localRepository.findByCidadeContaining(cidade).forEach(locais::add);
            if (locais.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(locais, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/local/{id}")
    public ResponseEntity<Local> getLocalById(@PathVariable("id") long id) {
        Optional<Local> localData = localRepository.findById(id);
        if (localData.isPresent()) {
            return new ResponseEntity<>(localData.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/locais")
    public ResponseEntity<Local> createLocal(@RequestBody Local local) {
        try {
            Local _local = localRepository
                    .save(new Local(local.getCidade(), local.getSetor(), local.getPico()));
            return new ResponseEntity<>(_local, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PutMapping("/locais/{id}")
    public ResponseEntity<Local> updateLocal(@PathVariable("id") long id, @RequestBody Local local) {
        Optional<Local> localData = localRepository.findById(id);
        if (localData.isPresent()) {
            Local _local = localData.get();
            _local.setCidade(local.getCidade());
            _local.setSetor(local.getSetor());
            _local.setPico(local.getPico());
            return new ResponseEntity<>(localRepository.save(_local), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    @DeleteMapping("/locais/{id}")
    public ResponseEntity<HttpStatus> deleteLocal(@PathVariable("id") long id) {
        try {
            localRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @DeleteMapping("/locais")
    public ResponseEntity<HttpStatus> deleteAllLocais() {
        try {
            localRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

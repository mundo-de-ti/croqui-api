package br.com.shazan.croqui.cocalzinho.model;

import javax.persistence.*;

@Entity
@Table(name = "local")
public class Local {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "cidade", nullable = false)
    private String cidade;
    @Column(name = "pico", nullable = false)
    private String pico;
    @Column(name = "setor", nullable = false)
    private String setor;
    public Local() {
    }
    public Local(String cidade, String pico, String setor) {
        this.cidade = cidade;
        this.pico = pico;
        this.setor = setor;
    }
    public Long getId() {
        return id;
    }
    public String getCidade() {
        return cidade;
    }
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }
    public String getPico() {
        return pico;
    }
    public void setPico(String pico) {
        this.pico = pico;
    }
    public String getSetor() {
        return setor;
    }
    public void setSetor(String setor) {
        this.setor = setor;
    }

    @Override
    public String toString() {
        return "Local [id=" + id + ", cidade=" + cidade + ", pico=" + pico + ", setor=" + setor + "]";
    }
}
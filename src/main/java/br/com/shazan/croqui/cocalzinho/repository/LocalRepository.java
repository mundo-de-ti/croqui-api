package br.com.shazan.croqui.cocalzinho.repository;

import br.com.shazan.croqui.cocalzinho.model.Local;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LocalRepository extends JpaRepository<Local, Long> {
//    List<Local> findByCidade(String cidade);
    List<Local> findByCidadeContaining(String cidade);
    List<Local> findByPicoContaining(String pico);
}